<?php
/**
 * Description of CommentModel.php
 * @author Twista
 * @package 
 */

class CommentModel extends Nette\Object{

    /** @var \Nette\Database\Table\Selection */
    protected $table;

    /**
     * @param \Nette\Database\Connection $conn
     */
    public function __construct(Nette\Database\Connection $conn){
        $this->table = $conn->table('comments');
    }

    /**
     * insert new comment
     * @param array[] $values
     * @return \Nette\Database\Table\ActiveRow
     */
    public function insertComment($values){
        return $this->table->insert($values);
    }

}